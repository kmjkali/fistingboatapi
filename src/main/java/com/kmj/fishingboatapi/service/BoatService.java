package com.kmj.fishingboatapi.service;

import com.kmj.fishingboatapi.entity.BoatType;
import com.kmj.fishingboatapi.model.BoatTypeRequest;
import com.kmj.fishingboatapi.repository.BoatTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Setter
@Service
@RequiredArgsConstructor
public class BoatService {

    private final BoatTypeRepository boatTypeRepository;

    public void setBoatType(BoatTypeRequest request){

        BoatType addData = new BoatType();

        addData.setName(request.getName());
        addData.setLength(request.getLength());
        addData.setWidth(request.getWidth());
        addData.setDepth(request.getDepth());
        addData.setTon(request.getTon());
        addData.setEngineOutput(request.getEngineOutput());

        boatTypeRepository.save(addData);
    }


}
