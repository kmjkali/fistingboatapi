package com.kmj.fishingboatapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoatTypeRequest {
    private String name;
    private Float length;
    private Float width;
    private Float depth;
    private Float ton;
    private Float engineOutput;
}
