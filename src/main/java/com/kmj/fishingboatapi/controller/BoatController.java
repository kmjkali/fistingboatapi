package com.kmj.fishingboatapi.controller;

import com.kmj.fishingboatapi.model.BoatTypeRequest;
import com.kmj.fishingboatapi.service.BoatService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/boat")

public class BoatController {
    private final BoatService boatService;

    @PostMapping("/new")
    public String setBoat(@RequestBody BoatTypeRequest request) {
        boatService.setBoatType(request);
        return "입력되었습니다";
    }
}
