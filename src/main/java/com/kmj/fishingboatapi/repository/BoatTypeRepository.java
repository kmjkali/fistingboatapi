package com.kmj.fishingboatapi.repository;

import com.kmj.fishingboatapi.entity.BoatType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoatTypeRepository extends JpaRepository<BoatType,Long> {
}
