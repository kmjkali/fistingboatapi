package com.kmj.fishingboatapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FishingboatApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FishingboatApiApplication.class, args);
    }

}
