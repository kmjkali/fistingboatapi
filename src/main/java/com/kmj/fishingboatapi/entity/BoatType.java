package com.kmj.fishingboatapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class BoatType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false, length = 25)
    private Float length;
    @Column(nullable = false)
    private Float width;
    @Column(nullable = false)
    private Float depth;
    @Column(nullable = false)
    private Float ton;
    @Column(nullable = false)
    private Float engineOutput;

}

   
